%% Random Clustering Ferns (RCFs)
function prg_rcfs_2d()
clc,close all,clear all

% messages
fun_messages('Random Clustering Ferns','presentation');
fun_messages('Random Clustering Ferns','title');

% global variables
global iterExp;

% experiments
for iterExp = 1:1
    
    % delete variables
    fun_delete_variables();
    
    % Random means: This function computes random class means in the
    % feature space.
    means = fun_random_means();  % positive class means
    
    % Samples: This function generates samples at random in the feature space.
    [trnSamples,trnLabels] = fun_random_samples(means,'train');  % training samples
    [tstSamples,tstLabels] = fun_random_samples(means,'test');  % test samples

    % Show samples: This shows the samples in the feature space. Only two
    % feature dimensions (x1,x2) are plotted.
    fun_show_samples(trnSamples,trnLabels,means,'train');  % training samples
    fun_show_samples(tstSamples,tstLabels,means,'test');  % test samples
    
    % Random clustering ferns (rcfs): This function computes the random 
    % clustering ferns using the training samples and their positive/negative 
    % class labels (not clusters labels).
    rcfs = fun_random_clustering_ferns(trnSamples,trnLabels,means);
    
    % Test: This function tests the random clustering ferns over the
    % input samples.
    trnRes = fun_test(rcfs,trnSamples,trnLabels);  % training samples
    tstRes = fun_test(rcfs,tstSamples,tstLabels);  % test samples
        
    % show results
    fun_show_results(trnRes,'RCFs - train samples');
    fun_show_results(tstRes,'RCFs - test samples');
    
    % save variables
    results.rcfs.train = trnRes;
    results.rcfs.test = tstRes;
    fun_data_save(results,'./variables/','results.mat');
    fun_data_save(rcfs,'./variables/','rcfs.mat');

    % results
    fun_save_results();
          
end

% message
fun_messages('end','title');
end
