%% Random means
% This function computes random clusters means in the feature space. 
% They are used to generate positive class samples, where each one belongs 
% to particular intra-class cluster or mode.
function output = fun_random_means()
if (nargin~=0), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
numDims = prms.samples.numDims;  % num. feature space dimensions (two by default)
clusThr = prms.samples.clusThr;  % cluster distance threshold
numClusters = prms.samples.numClusters;  % num. intra-class clusters (positive samples)

% messages
fun_messages('random means','process');
fun_messages(sprintf('num. means: %d',numClusters),'information');

% load/compute random means
try

	% load previous random means
	means = fun_data_load('./variables/','means.mat');

catch ME

	% allocate
	means = zeros(numClusters,numDims);

	% random means
	for iterMean = 1:numClusters
		% distance 
		dist = 0;
		% check distance to previous clusters means
		while (dist<clusThr)
			% random mean
			mean = rand(1,numDims);
			% distance metric
			for iter = 1:iterMean
				dist = sum(abs(mean - means(iter,:)),2);
				% check
				if (dist<clusThr), break; end
			end
			% for the first mean
			if (iterMean==1),dist = 1; end
		end
		% clusters means
		means(iterMean,:) = mean;
	end
        
	% save random means
	fun_data_save(means,'./variables/','means.mat');

end

% output
output = means;
end
