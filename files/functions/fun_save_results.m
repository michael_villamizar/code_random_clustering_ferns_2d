%% Save results
% This function saves the experiment results in a particular folder.
function fun_save_results()
    
% current experiment
prmsExp = fun_experiments();
  
% output directory    
mkdir(sprintf('./results/%s',prmsExp.tag));    
mkdir(sprintf('./results/%s/variables/',prmsExp.tag));    

% move variables
movefile('./variables/*.mat',sprintf('./results/%s/variables/',prmsExp.tag));

end
