%% Load data
% This function loads a *.mat file.
function output = fun_data_load(path,name)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% load file
data = load([path,name]);

% output
output = data.data;
end
