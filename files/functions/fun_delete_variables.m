%% Delete variables
% This function removes the temporal variables.
function fun_delete_variables()
% delete
delete './variables/*.mat';
end
