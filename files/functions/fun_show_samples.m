%% Show samples
% This function shows the samples in the feature space. For cases with more 
% than two feature dimensions, this function only plots the first two feature
% dimensionsi.
function fun_show_samples(samples,labels,means,txt)
if (nargin~=4), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fts = prms.visualization.fontSize;  % font size
mks = prms.visualization.markerSize;  % marker size
lnw = prms.visualization.lineWidth;  % line width

% num. samples and intra-class clusters
numSamples = size(samples,1);
numClusters = size(means,1);

% show samples -only two first dimensions-
figure,grid on, axis([0,1,0,1]),hold on;
for iter = 1:numSamples

	% sample properties
	x1 = samples(iter,1);  % dim. x1
	x2 = samples(iter,2);  % dim. x2
	label = labels(iter,:);  % cluster label

	% Plot sample: Positive samples are shown in cyan crosses
        % and black samples in black circles.
	if (label>0),plot(x1,x2,'+','color',fun_colors(1),'markersize',...
		mks,'linewidth',lnw),hold on; end
	if (label<0),plot(x1,x2,'o','color',fun_colors(-1),'markersize',mks,...
		'linewidth',lnw),hold on; end
end
xlabel('x1','fontsize',fts),ylabel('x2','fontsize',fts);
title(sprintf('%s samples',txt),'fontsize',fts);

% legend
text(0.05,0.15, 'Positive','color','k','backgroundcolor',fun_colors(1),...
	'edgecolor',fun_colors(1),'fontsize',fts,'fontweight','bold');
text(0.05,0.05, 'Negative','color','k','backgroundcolor',fun_colors(-1),...
	'edgecolor',fun_colors(-1),'fontsize',fts,'fontweight','bold');
end
