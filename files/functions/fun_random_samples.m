%% Random samples
% This function generates positive and negative samples at random in the
% feature space. Positive samples are computed according to the input clusters
% means, whereas the negative ones are computed at random over the whole
% feature space. This function returns the samples and their corresponding
% true labels. Although the random clustering ferns (rcfs) work without using 
% the labels for the intra-class clusters, since rcfs discover these labels 
% automatically during the training step, in this function we save the clusters
% labels for evaluation and visualization purposes.
function [output1,output2] = fun_random_samples(means,txt)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters;  % program parameters
clusThr = prms.samples.clusThr;  % cluster distance threshold
numDims = prms.samples.numDims;  % num. feature space dimensions (2 by default)
clusStd = prms.samples.clusStd;  % cluster standard deviation
numClusters = prms.samples.numClusters;  % num. positive classes
numPosSamples = prms.samples.numPosSamples;  % num. positive class samples
numNegSamples = prms.samples.numNegSamples;  % num. negative class samples

% message
fun_messages(sprintf('%s samples',txt),'process');
fun_messages(sprintf('num. clusters: %d',numClusters),'information');
fun_messages(sprintf('num. positive samples: %d',numPosSamples),'information');
fun_messages(sprintf('num. negative samples: %d',numNegSamples),'information');

% load/compute samples
try
    
	% load previous samples and labels
	samples = fun_data_load('./variables/',sprintf('%s_samples.mat',txt));
	labels = fun_data_load('./variables/',sprintf('%s_labels.mat',txt));
    
catch ME
    
	% random clusters indexes -positives-
	indxs = max(1,min(numClusters,ceil(numClusters*rand(numPosSamples,1))));
    
	% allocate
	posLabels = zeros(numPosSamples,1);  % positive samples labels -cluster index-
	posSamples = zeros(numPosSamples,numDims);  % positive samples data -points-
	negLabels = zeros(numNegSamples,1);  % negative samples labels -cluster index-
	negSamples = zeros(numNegSamples,numDims);  % negative samples data -points-
    
	% positive samples
	for iterSample = 1:numPosSamples
		% cluster label
		label = indxs(iterSample);
		% positve sample
		posSamples(iterSample,:) = means(label,:) + clusStd.*randn(1,numDims);
		% sample label
		posLabels(iterSample,1) = label;
	end

	% negative samples
	for iterSample = 1:numNegSamples
		% distance variable
		dist = 0;
		% check distance to intra-class clusters means 
		while (dist<clusThr)
			% random sample
			sample = rand(1,numDims);
			% distance metric
			for iterMean = 1:size(means,1)
				tmp = abs(sample - means(iterMean,:));
				dist = sqrt(sum(tmp.*tmp,2));
				% check
				if (dist<clusThr), break; end
			end
		end
		% negative sample
		negSamples(iterSample,:) = sample;
		% sample label
		negLabels(iterSample,1) = -1;
	end
    
	% samples and labels
	samples = [posSamples;negSamples];
	labels = [posLabels;negLabels];

	% random order
	indxs = randperm(size(samples,1));
	samples = samples(indxs,:);
	labels = labels(indxs,:);

	% save samples and labels
	fun_data_save(samples,'./variables/',sprintf('%s_samples.mat',txt));
	fun_data_save(labels,'./variables/',sprintf('%s_labels.mat',txt));
end

% output
output1 = samples;
output2 = labels;
end
