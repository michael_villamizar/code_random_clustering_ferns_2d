%% Fern outputs
% This function computes the fern outputs over the input sample.
function output = fun_fern_outputs(sample,ferns)

% parameters
numFerns = size(ferns,1);  % num. ferns
numFeats = size(ferns,2);  % num. fern features

% allocate
data = zeros(1,numFerns);

% feature sets -random ferns-
for iterFern = 1:numFerns
    
    % fern output
    z = 1;
    
    % features
    for iterFeat = 1:numFeats
        
        % feature and threshold
        x = ferns(iterFern,iterFeat,1);
        t = ferns(iterFern,iterFeat,2);
        
        % sample value
        value = sample(1,x);
        
        % update output
        if (value>t), z = z + 2^(iterFeat-1); end
        
    end
    
    % save
    data(1,iterFern) = z;
    
end

% output
output = data;
end
