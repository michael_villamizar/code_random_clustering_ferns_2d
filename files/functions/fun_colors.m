%% Colors
% This function returns a color vector according to the input value.
function output = fun_colors(index)
if (nargin~=1), fun_messages('incorrect input variables','error'); end

% colors
switch (index)
case -1
        c = [0,0,1];
case 1
	c = [1,0.7,0];
case 2
	c = [1,0,1];
case 3
	c = [0.5,0.5,0.3];
case 4
	c = [0.2,0.8,0.3];
case 5
	c = [0.1,0.8,0.8];
case 6
	c = [1.0,0.2,0.1];
case 7
	c = [0.8,0.8,1];
case 8
	c = [0.0,0.0,1.0];
case 9
	c = [0.9,0.7,0.7];
case 10
	c = [0.2,0.3,0.4];
otherwise
	c = rand(1,3);
end
        
% output
output = c;
end
