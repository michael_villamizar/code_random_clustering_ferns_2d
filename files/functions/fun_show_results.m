%% Classification results
% This function shows the classification results on the input samples.
function fun_show_results(results,text)

% parameters
prms = fun_parameters();  % program parameters
fs = prms.visualization.fontSize;  % visualization: font size
lw = prms.visualization.lineWidth;  % visualization: line width
ms = prms.visualization.markerSize;  % visualization: marker size
pc = prms.visualization.posColor;  % visualization: positive color
nc = prms.visualization.negColor;  % visualization: negative color
ds = prms.visualization.distSize;  % visualization: distribution size      

% results
eer = results.eer;  % equal error rate
thr = results.thr;  % classification threshold
ind = results.idx;  % threshold index
dst = results.dst;  % distributions distance
lbl = results.lbl;  % true sample labels
est = results.est;  % estimated sample labels
rec = results.curves.rec;  % recall curve
pre = results.curves.pre;  % precision curve
fme = results.curves.fme;  % f-measure curve
tps = results.curves.tps;  % true positives curve
fps = results.curves.fps;  % false positives curve        
fns = results.curves.fns;  % false negatives curve
cnfMat = results.cnfMat;  % confusion matrix
cnfEnt = results.cnfEnt;  % entropy metric (conf. matrix)
posMiu = results.distrs.posMiu;  % positive class mean
negMiu = results.distrs.negMiu;  % negative class mean
posVar = results.distrs.posVar;  % positive class variance
negVar = results.distrs.negVar;  % negative class variance
scores = results.scores.samples;  % classification scores
samples = results.samples;  % samples
posScores = results.scores.positive;  % negative scores
negScores = results.scores.negative;  % positive scores

% variables
numSamples = size(samples,1);  % num. samples

%% messages
fun_messages(sprintf('Test: %s results',text),'process');
fun_messages(sprintf('equal error rate: %.3f',eer),'information');
fun_messages(sprintf('classifier threshold: %.3f',thr),'information');
fun_messages(sprintf('distributions distance:  %.3f',dst),'information');
fun_messages(sprintf('clustering error: %.3f',cnfEnt),'information');

%% show precision-recall plot
figure,subplot(131),plot(rec,pre,'m-','linewidth',lw),grid on,hold on;
line([0,1],[0,1],'color','k'); 
title('Precision-Recall Plot','fontsize',fs);
xlabel('Recall','fontsize',fs),ylabel('Precision','fontsize',fs);
legend(sprintf('EER: %.3f',eer));

%% show positive and negative class scores
subplot(132),grid on,hold on;
plot(posScores,'-o','color',pc,'linewidth',lw),hold on;
plot(negScores,'-*','color',nc,'linewidth',lw), hold on
grid on, hold on;
title('Score Performance','fontsize',fs),hold on
xlabel('# Samples','fontsize',fs),ylabel('Score','fontsize',fs);
legend('Positive','Negative');

%% show positive and negative score distributions 

% min. and max. score values
minScore = min(scores(:));
maxScore = max(scores(:));

% variables
k = sqrt(2*pi);  % constant
s = minScore:inv(ds-1):maxScore;    % scores

% positive and negative gaussian distributions
p = 1/(ds*k*sqrt(posVar))*exp(-0.5*((s-posMiu).^2)./posVar);
n = 1/(ds*k*sqrt(negVar))*exp(-0.5*((s-negMiu).^2)./negVar);

% plot score distributions
subplot(133),plot(s,p,'-o','color',pc,'linewidth',lw), hold on;
plot(s,n,'-*','color',nc,'linewidth',lw'), hold on;

% messages
title(sprintf('Distributions Distance: %.3f',dst),'fontsize',fs);
xlabel('Score','fontsize',fs),ylabel('Probability','fontsize',fs);
legend(sprintf('Pos. - mean %.2f - var. %.4f',posMiu,posVar),...
        sprintf('Neg. - mean %.2f - var. %.4f',negMiu,negVar));
legend('Pos. Distribution','Neg. Distribution')

%% show classification results

% figure
figure,axis([0,1,0,1]),hold on,grid on

% samples
for iterSample = 1:numSamples
    
    % sample properties
    truLabel = lbl(iterSample);   % true label
    estLabel = est(iterSample);   % estimated label
   
    % plot sample
    if (truLabel>0)
        if (estLabel>0)
            % true positive
            plot(samples(iterSample,1),samples(iterSample,2),'+','color',fun_colors(estLabel),...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false negative
            plot(samples(iterSample,1),samples(iterSample,2),'+','color',[0,0,0],...
                'markersize',ms,'linewidth',lw),hold on
        end
    else
        if (estLabel<0)
            % true negative
            plot(samples(iterSample,1),samples(iterSample,2),'o','color',fun_colors(estLabel),...
                'markersize',ms,'linewidth',lw),hold on
        else
            % false positive
            plot(samples(iterSample,1),samples(iterSample,2),'o','color',[0,0,0],...
                'markersize',ms,'linewidth',lw),hold on
        end
    end
end

% messages
title(sprintf('Classification Results - %s',text),'fontsize',fs), hold on;
xlabel('x1'), hold on;
ylabel('x2'), hold on;
end
