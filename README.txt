Random Clustering Ferns 2D

 Description: 
   This code computes Random Clustering Ferns (RCFs) to classify and clustering
   two different classes (positive and negative classes) with multiple 
   intra-class modes in a two-dimensional feature space (2D). 
   In particular, RCFs use Boosted Random Ferns (BRFs) and probabilistic Latent
   Semantic Analysis (pLSA) to obtain a discriminative and multimodal classifier
   that automatically clusters the 2D samples using the response of the 
   randomized trees.  

   For more detail refer to reference [1].

 Comments:
   The parameters of the method and the 2D feature scenario can be found 
   in the fun_parameters.m function.

   In this code, the training and test samples are generated at random. The
   positive samples correspond to K different clusters whereas the negative
   samples are spread over the entire 2D feature space.

   This code does not include the online classifier described in [1] to collect  
   training samples. Here, the samples are generated at random. This code 
   focuses mainly on the computation and evaluation of random clustering ferns.

   This code downloads and uses the public pLSA implementation developed by 
   Josef Sivic [http://www.robots.ox.ac.uk/~vgg/software]/. 

   If you make use of this code for research articles, we kindly encourage
   to cite the reference [1], listed below. This code is only for research 
   and educational purposes.

 Steps:
   Steps to exucute the program:
     1. Run the prg_setup.m file to configure the program paths.
     2. Run the prg_rcfs_2d.m file to compute the random clustering ferns. 

 References:
   [1] Multimodal Object Recognition using Random Clustering Trees
       M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
       Iberian Conference on Pattern Recognition and Image Analysis (IbPRIA)
       Santiago de Compostela, Spain, June 2015 

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2016
